# Hiring Creditoo

Hello developer! 

Do you want to work with us? If yes, *Fork* this repository, complete our test and open a PR with your code.

Also, we need you to send us your resume to wesckley.uno@creditoo.com.br and, please, send all stuff you think it's interesting about you too: linkedin, twitter, blog, site, github, open-source projects that you participate, articles that you have wrote, etc. Everything you judge good about you.


## Requirements

- Laravel 5+
- Make an unit test for each end-point
- Docker
- Khow how to work with Linux
- Khow how to work with GIT
- Like to be challenged
- Love to code (a lot)!


## The Test

Using the requirements, create a RESTful API to retrieve a resource about a github account. 

#### End-point list

**Get user profile**

Request

GET localhost/api/users/{username}

Response

json structure must contain
```
{
    "id",
    "login",
    "name",
    "avatar_url",
    "html_url"
}
```
 
**Get user repos**

Request

GET localhost/api/users/{username}/repos

Response

json structure must contain

```
[
    {
        "id",
        "name",
        "description",
        "html_url"
    }, {
        "id",
        "name",
        "description",
        "html_url"
    }, {
        "id",
        "name",
        "description",
        "html_url"
    }, {
        ...
    }
]
```

Remember to comment the code and commits preferably in english and write the project *README* which must contain the steps to install and run the app on a debian based linux machine (debian, ubuntu, etc).

Good lucky :wink: