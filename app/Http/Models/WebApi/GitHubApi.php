<?php

namespace App\Http\Models\WebApi;

use GuzzleHttp\Client;



class GitHubApi 
{

    public $client;
    public $githubkey;
    public $gitUrl;
    
    public function __construct(Client $client){
        $this->client=$client;
        $this->githubkey=env("GITHUB_KEY");
        $this->gitUrl="https://api.github.com";
    }


    public function getUserInfo($user){
        $url=$this->gitUrl."/users/".$user."?access_token=".$this->githubkey;
        $request=$this->client->request("GET",$url);
        return $request->getBody();
    }

    public function getUserRepo($user){
        $url=$this->gitUrl."/users/".$user."/repos?access_token=".$this->githubkey;
        $request=$this->client->request("GET",$url);        
        return $request->getBody();
    }
}
