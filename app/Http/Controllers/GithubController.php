<?php

namespace App\Http\Controllers;

use App\Http\Models\WebApi\GitHubApi;

class GitHubController extends Controller
{
    public $gitHubApi;

    public function __construct(GitHubApi $gitHubApi){
        $this->gitHubApi=$gitHubApi;
    }

    public function returnUserInfo($user){
        echo $this->gitHubApi->getUserInfo($user);
    }

    public function returnUserRepo($user){
        echo $this->gitHubApi->getUserRepo($user);
    }
}
